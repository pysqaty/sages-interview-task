import tensorflow as tf
import transformers as trfs

class TwitterSentimentClassifier:
    def __init__(self, config):
        self.config = config
        self.tokenizer = trfs.BertTokenizer.from_pretrained(self.config['pretrained_model'])
        self.model = self._build()
        self._compile()

    def _build(self):
        bert_model = trfs.TFBertForSequenceClassification.from_pretrained(self.config['pretrained_model'], num_labels=self.config["num_labels"])
        
        input_ids = tf.keras.layers.Input(shape=(self.config['max_length'],), dtype=tf.int32, name='input_ids')
        attention_mask = tf.keras.layers.Input((self.config['max_length'],), dtype=tf.int32, name='attention_mask')

        output = bert_model([input_ids, attention_mask])[0] 
        output = tf.keras.layers.Dropout(rate=0.15)(output)
        output = tf.keras.layers.Dense(self.config["num_labels"], activation='softmax',
                kernel_regularizer=tf.keras.regularizers.l1_l2(l1=self.config['l1'], l2=self.config['l2']))(output)

        model = tf.keras.models.Model(inputs=[input_ids, attention_mask], outputs=output)
        return model

    def train(self, train_data, val_data):
        X_train = self._batch_encode(train_data[0], self.tokenizer, self.config['max_length'])
        X_val = self._batch_encode(val_data[0], self.tokenizer, self.config['max_length'])

        self.model.fit(
            x=X_train.values(),
            y=train_data[1],
            validation_data=(X_val.values(), val_data[1]),
            epochs=self.config['epochs'],
            batch_size=self.config['batch_size']
        )

    def predict(self, data):
        data = self._batch_encode(data, self.tokenizer, self.config['max_length'])
        y_pred = self.model.predict(data.values())
        y_pred = tf.nn.softmax(y_pred, axis=1).numpy()
        y_pred = y_pred.argmax(axis = 1)
        return y_pred

    def load_model(self, path):
        self.model.load_weights(path)

    def save_model(self, path):
        self.model.save_weights(path)

    def _compile(self):
        opt = tf.keras.optimizers.Adam(learning_rate=self.config['learning_rate'])
        self.model.compile(optimizer=opt, loss='sparse_categorical_crossentropy', metrics=['accuracy'])

    def _batch_encode(self, X, tokenizer, max_length):
        return tokenizer.batch_encode_plus(
            X,
            max_length=max_length,
            add_special_tokens=True,
            return_attention_mask=True,
            return_token_type_ids=False,
            pad_to_max_length=True,
            return_tensors='tf'
        )

    