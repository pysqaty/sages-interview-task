import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import nltk
from collections import defaultdict
import heapq
import operator
from wordcloud import WordCloud, STOPWORDS

def show_wordcloud(data):
    stopwords = set(STOPWORDS)
    wordcloud = WordCloud(
        background_color='white',
        stopwords=stopwords,
        max_words=100,
        max_font_size=30,
        scale=3,
        random_state=1)
   
    wordcloud=wordcloud.generate(str(data))

    fig = plt.figure(1, figsize=(12, 12))
    plt.axis('off')

    plt.imshow(wordcloud)
    plt.show()

def main(args):
    df = pd.read_csv(args.input)
    # Print balance of the dataset
    print(df['label'].value_counts())

    # Show tweets length distribution
    n, _, _ = plt.hist(x=df['length'], bins='auto', color='#0504aa',
                            alpha=0.7, rwidth=0.85)
    plt.grid(axis='y', alpha=0.75)
    plt.xlabel('Length')
    plt.ylabel('Frequency')
    plt.title('Tweets length distribution')
    plt.text(23, 45, r'$\mu=15, b=3$')
    maxfreq = n.max()
    plt.ylim(ymax=np.ceil(maxfreq / 10) * 10 if maxfreq % 10 else maxfreq + 10)
    plt.show()
    plt.clf()

    # Show tweets word count distribution
    df['word_count'] = df['text'].apply(lambda x: len(x.split()))
    n, _, _ = plt.hist(x=df['word_count'], bins='auto', color='#0504aa',
                        alpha=0.7, rwidth=0.85)
    plt.grid(axis='y', alpha=0.75)
    plt.xlabel('Length')
    plt.ylabel('Frequency')
    plt.title('Tweets word count distribution')
    plt.text(23, 45, r'$\mu=15, b=3$')
    maxfreq = n.max()
    plt.ylim(ymax=np.ceil(maxfreq / 10) * 10 if maxfreq % 10 else maxfreq + 10)
    plt.show()
    plt.clf()

    # Show average word length distribution
    df['avg_word_len'] = df['text'].apply(lambda x: [len(i) for i in x.split()]).map(lambda x: np.mean(x))
    n, _, _ = plt.hist(x=df['avg_word_len'], bins='auto', color='#0504aa',
                        alpha=0.7, rwidth=0.85)
    plt.grid(axis='y', alpha=0.75)
    plt.xlabel('Length')
    plt.ylabel('Frequency')
    plt.title('Average word length distribution')
    plt.text(23, 45, r'$\mu=15, b=3$')
    maxfreq = n.max()
    plt.ylim(ymax=np.ceil(maxfreq / 10) * 10 if maxfreq % 10 else maxfreq + 10)
    plt.show()
    plt.clf()

    nltk.download('stopwords')
    stop=set(nltk.corpus.stopwords.words('english'))
    text =  df['text'].str.split()
    text = text.values.tolist()
    corpus = [word for i in text for word in i]
    dic=defaultdict(int)
    for word in corpus:
        if word in stop:
            dic[word]+=1
    n = 20
    topitems = heapq.nlargest(n, dic.items(), key=operator.itemgetter(1))
    topitemsasdict = dict(topitems)
    plt.bar(list(topitemsasdict.keys()), topitemsasdict.values(), color='g')
    # plt.show()
    plt.clf()

    text =  df['text'].str.split()
    text = text.values.tolist()
    corpus = [word for i in text for word in i]
    # show_wordcloud(corpus)
    plt.clf()

    df_n = df[df['label'] == 0]
    text =  df_n['text'].str.split()
    text = text.values.tolist()
    corpus_n = [word for i in text for word in i]
    # show_wordcloud(corpus_n)
    plt.clf()
    df_p = df[df['label'] == 1]
    text =  df_p['text'].str.split()
    text = text.values.tolist()
    corpus_p = [word for i in text for word in i]
    # show_wordcloud(corpus_p)
    plt.clf()



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", help="File path", 
                            type=str, required=True)
    args = parser.parse_args()
    main(args)

