import argparse
import pandas as pd
import sklearn.model_selection as ms
from sklearn.metrics import recall_score, precision_score, \
    accuracy_score, f1_score
import json
from model import TwitterSentimentClassifier

import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"



def print_metrics(y_true, y_pred):
    print("Negative precision: ", precision_score(y_pred, y_true, pos_label=0))
    print("Positive precision: ", precision_score(y_pred, y_true, pos_label=1))
    print("Negative recall: ", recall_score(y_pred, y_true, pos_label=0))
    print("Positive recall: ", recall_score(y_pred, y_true, pos_label=1))
    print("Negative f1: ", f1_score(y_pred, y_true, pos_label=0))
    print("Positive f1: ", f1_score(y_pred, y_true, pos_label=1))
    print("Accuracy: ", accuracy_score(y_pred, y_true))

def main(args):
    config_file = open(args.config)
    config = json.load(config_file)
    df = pd.read_csv(args.input)
    classifier = TwitterSentimentClassifier(config)

    if args.model_path:
        classifier.load_model(args.model_path)

    if args.action == 'train':
        X_train, X_val, y_train, y_val = ms.train_test_split(df.text.values, df.label.values, test_size=args.validation_ratio)
        classifier.train((X_train, y_train), (X_val, y_val))
        if args.output_model_path:
            os.makedirs(args.output_model_path, exist_ok=True)
            classifier.save_model(args.output_model_path)
        preds = classifier.predict(X_val)
        print_metrics(y_val, preds)
    elif args.action == 'predict':
        X_test = df['text'].to_numpy()
        y_test = df['label'].to_numpy()
        preds = classifier.predict(X_test)
        print_metrics(y_test, preds)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", help="Raw data file path", 
                            type=str, required=True)
    parser.add_argument("--model-path", help="Model weights path to load", 
                            type=str, required=False)
    parser.add_argument("--output-model-path", help="Model weights path to save", 
                            type=str, required=False)
    parser.add_argument("--config", help="Config path", 
                            type=str, required=True)
    parser.add_argument("--validation-ratio", type=tuple, default=0.1)
    parser.add_argument("--action", choices=['train', 'predict'], required=True)
    args = parser.parse_args()
    main(args)