import argparse
import pandas as pd
import re

def replace_mentions(df):
    mentions = re.compile(r"^@\S+|\s@\S+")
    df.text = df.text.apply(lambda x : mentions.sub('[mention]', x))

def replace_hashtags(df):
    hashtags = re.compile(r"^#\S+|\s#\S+")
    df.text = df.text.apply(lambda x : hashtags.sub('[hashtag]', x))

def replace_urls(df):
    urls = re.compile(r"https?://\S+")
    df.text = df.text.apply(lambda x : urls.sub('[url]', x))

def main(args):
    df = pd.read_csv(args.input, header=None, 
            index_col=0, names=['polarity', 'id', 'date', 'query', 'user', 'text'])
    df['length'] = df['text'].apply(lambda t : len(t))
    # 0 - negative, 1 - positive
    df['label'] = df['polarity'].apply(lambda x : int(x/4))
    df = df[['text', 'label', 'length']]
    df.dropna(inplace=True)
    if args.replace_mentions:
        replace_mentions(df)
    if args.replace_hashtags:
        replace_hashtags(df)
    if args.replace_urls:
        replace_urls(df)
    if args.drop_duplicates:
        df.drop_duplicates(subset=['text'], keep="last", inplace=True)
    if args.shuffle:
        df = df.sample(frac=1.0)
    if args.limit:
        df = df.head(int(args.limit))

    if args.case == "upper":
        df['text'] = df['text'].str.upper()
    elif args.case == "lower":
        df['text'] = df['text'].str.lower()

    df.to_csv(args.output, index=False)

    if args.split_data:
        split_limit = int(args.split_ratio * len(df))
        df_train = df.iloc[:split_limit]
        df_test = df.iloc[split_limit:]
        df_train.to_csv(args.output[:-4] + "_train.csv", index=False)
        df_test.to_csv(args.output[:-4] + "_test.csv", index=False)



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input", help="Raw data file path", 
                            type=str, required=True)
    parser.add_argument("--output", help="Processed data file path", 
                            type=str, required=True),
    parser.add_argument("--replace-mentions", action='store_true')
    parser.add_argument("--replace-hashtags", action='store_true')
    parser.add_argument("--replace-urls", action='store_true')
    parser.add_argument("--drop-duplicates", action='store_true')
    parser.add_argument("--shuffle", action='store_true')
    parser.add_argument("--limit", default=None)
    parser.add_argument("--split-data", action='store_true')
    parser.add_argument("--split-ratio", type=float, default=0.8)
    parser.add_argument("--case", choices=["keep", "lower", "upper"], default="keep")

    args = parser.parse_args()
    main(args)