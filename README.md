# Twitter sentiment analysis

## Usage

To preprocess the data you can use *data_processor.py* script. Sample call:

python data_processor.py --input ..\res\Data_tweets.csv --output ..\res\tweets.csv --case lower --replace-mentions --split-data 

You can use runner.py script for training and prediction. Sample training call:

python runner.py --input ..\res\tweets_train.csv --action train --config ..\res\config.json --output-model-path ..\res\model\  

And prediciton example:

python runner.py --input ..\res\tweets_test.csv --action predict --config ..\res\config.json --model-path ..\res\model\ 

## Dataset

We should start working on data with performing some analysis of the dataset.

First thing we had to check was dataset balance. As it turned out, the provided dataset is more or less balanced, so we don't have to focus on any technique to fight such an issue.

Whole dataset contains **15049** observations of *negative* tweets and **14951** of positive. Although, some duplicates have been spotted and after removing them class populations where equal to **14967** and **14817**. The numbers are still balanced enough. (However in the future it may be some idea to make it perfectly equal)

### Lengths and counts

![plot1](./images/length_dist.png)
![plot2](./images/word_count_dist.png)

As it can be seen, in general max tweets lengths is about 150 letters and 32 words. This information gives us some intuition about length related parameters we should use for our model.

![plot3](./images/avg_word_len.png)

Moreover, word used by users are in general pretty short. Therefore, we can calculate about 2 subwords per word and we are going to use max length hyperparameter equal to some value near 64 (32*2).

![plot4](./images/most-often-stopwords.png)

Mostly as interesting statistic, we can see what stopwords are the most common. It was expected and we don't think there is anything surprising about them.

### Wordclouds

Interesting plot we can take a look at are wordclouds. We can see what words were used the most often.

![plot5](./images/wordcloud-whole.png)

We can observe that there is a lot of twitter specific or internet related words, that would not be that often in some other texts, i.e. "hashtag", "mention", "lol" or even "twitter" itself.

Next, we splitted data for negative and positive classes and plot wordclouds of only one class. In such way, we are able to gain some intuition what words might be informative in order to classify whole text to one class. 

![plot6](./images/positive.png)

Tweets with positive sentiment have a lot of very positive words, such as "great", "love" and "thanks". It may led us to conclusion that the task will be rather easy.

![plot7](./images/negative.png)

Surprisingly, negative texts apart from expected word like "sad", "miss" or "hate" contains a lot of rather positive words, for example "wish" or "good". Maybe, because it seems more popular to say "not good" = "bad" than using "not bad" that doesn't seem to be as natural (it reminds some kind of double negation).

In addition, we have to remember that quality of the dataset might not be the highest, as it was not annotated manually. Generations was performed with use of emoticons and their meanings.

## Preprocessing

Tweets contains a lot of special words related to Twitter mechanism. They can contain hashtags (#...), mentions of other users (@...) and urls (http...). We may consider removing them and replacing, so for example, our model will not learn mostly meaningless usernames.

Removing hashtags can also be examined, but in general hashtags should be strongly related to whole text of the tweet.

## Model

It was decided to use deep learning model based on BERT for English. Whole dataset can be found on the internet and it contanes about 1.6 million observations, so using such a big architecture should not be problem for perspective of gained dataset.

## Results

Firstly, it was tried to remove all special kinds of words (mentions, hashtags and urls)

| Metric | Negative (0)  | Positive (1) |
| :---:   | :-: | :-: |
| Precision | 82.79 | 82.00 |
| Recall | 82.30 | 82.50 |
| F1 | 82.55 | 82.25 |
**Accuracy** = 82.4%

As hashtags and urls can be pretty informative, we have tried to use data with mentions removed only. Here are results:

| Metric | Negative (0)  | Positive (1) |
| :---:   | :-: | :-: |
| Precision | 83.32 | 82.04 |
| Recall | 82.42 | 82.95 |
| F1 | 82.87 | 82.49 |
**Accuracy** = 82.68%

There is very small gain in metrics, probably insignificant, but the results were not worse, so it may be step in good direction.

Just to be sure, that max length of sequences set to 64 is enough, it was tested with max_length=140.


| Metric | Negative (0)  | Positive (1) |
| :---:   | :-: | :-: |
| Precision | 84.89 | 79.20 |
| Recall | 81.63 | 82.80 |
| F1 | 83.23 | 80.96 |
**Accuracy** = 82.17%

As we can see, results were definitely not better. For the first time, one of the metrics was below 80%.

# Future

For sure, there is much more to do in order to obtain better results.

We should work on right regularizations techniques in order to avoid overfitting that was spotted right now. Of course, hyperparameters optimization should be performed too. Maybe some different classifier heads should be tested.
